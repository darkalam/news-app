package com.example.news.utils

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity


class NavigationUtils{

    companion object{

        fun navigate(activity: AppCompatActivity,javaClass: Class<out Activity>) {
            val navigationIntent = Intent(activity, javaClass)
            activity.startActivity(navigationIntent)
        }
    }
}
