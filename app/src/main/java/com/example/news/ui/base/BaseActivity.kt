package com.example.news.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        initData()
        initView()
        start()
    }


    /**
     *  GET LAYOUT
     */
    abstract fun layoutId(): Int

    /**
     * INITDATA
     */
    abstract fun initData()

    /**
     * INIT ViewS
     */
    abstract fun initView()

    /**
     * START ACTIVITY
     */
    abstract fun start()

}