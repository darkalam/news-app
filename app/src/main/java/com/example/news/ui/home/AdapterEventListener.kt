package com.example.news.ui.home

import com.example.news.models.Article

interface AdapterEventListener {
    fun onNewsClickListener(article: Article)
}