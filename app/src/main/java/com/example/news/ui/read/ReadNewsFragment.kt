package com.example.news.ui.read

import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.news.R
import com.example.news.models.Article
import com.example.news.ui.base.BaseFragment
import com.example.news.ui.detail.WebViewActivity
import com.example.news.ui.home.AdapterEventListener
import com.example.news.ui.home.NewsAdapter
import com.example.news.utils.AppUtils
import kotlinx.android.synthetic.main.fragment_read_news.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class ReadNewsFragment : BaseFragment(), AdapterEventListener {

    private val readNewsViewModel by viewModel<ReadNewsViewModel>()
    private lateinit var newsAdapter: NewsAdapter
    private var articleList : MutableList<Article>? = mutableListOf()

    override fun getLayoutId(): Int {
        return R.layout.fragment_read_news
    }

    override fun initView() {
        multipleStatusView.showLoading()
    }

    override fun lazyLoad() {
        readNewsViewModel.bookmarkNewsLive.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()){
                multipleStatusView.showEmpty()
            }else{
                articleList!!.addAll(it)
                multipleStatusView.showContent()
                mRecyclerView.layoutManager= LinearLayoutManager(requireContext())
                newsAdapter= NewsAdapter(articleList!!,this)
                mRecyclerView.adapter=newsAdapter
            }
        })
    }

    override fun onNewsClickListener(article: Article) {
        Timber.e("Item Clicked ${article.author}")
        val builder = CustomTabsIntent.Builder()
        builder.setToolbarColor(ContextCompat.getColor(this.requireContext(), R.color.colorPrimary))
        builder.setShowTitle(true)
        builder.setStartAnimations(this.requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)
        builder.setExitAnimations(this.requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)

        val customTabsIntent = builder.build()
        // check is chrom available
        val packageName = AppUtils.getPackageNameToUse(this.requireContext(), article.url!!)

        when {
            packageName != null -> {
                customTabsIntent.intent.setPackage(packageName)
                customTabsIntent.launchUrl(this.requireContext(), Uri.parse(article.url))
            }
            else -> {
                val webViewIntent = Intent(requireContext(), WebViewActivity::class.java)
                webViewIntent.putExtra("url", article.url)
                startActivity(webViewIntent)
            }
        }
    }
}