package com.example.news.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.news.R
import com.example.news.databinding.ItemNewsBinding
import com.example.news.models.Article
import com.example.news.utils.AppUtils
import timber.log.Timber

class NewsAdapter(private val newsArticles:List<Article>, private val  adapterEventListener: AdapterEventListener): RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
      return NewsViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context),
          R.layout.item_news,parent,false))
    }

    override fun getItemCount(): Int {
       return newsArticles.size
    }


    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val article=newsArticles[position]
        if (article.isBookmark){
            if (position % 4 == 1) {
                val articleTime = AppUtils.formatDate(article.publishedAt)
                Timber.e("article time $articleTime")
                article.publishedAt = articleTime
                holder.itemNewsBinding.article = article
                holder.itemNewsBinding.line1.setOnClickListener { adapterEventListener.onNewsClickListener(article) }
            } else {
                val articleTime = AppUtils.formatDate(article.publishedAt)
                Timber.e("article time $articleTime")
                article.publishedAt = articleTime
                holder.itemNewsBinding.article = article
                holder.itemNewsBinding.line2.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
                holder.itemNewsBinding.line3.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
                holder.itemNewsBinding.line4.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
                holder.itemNewsBinding.line5.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
            }
        }else{
            if (position % 4 == 1) {
                val articleTime = AppUtils.formatDate(article.publishedAt)
                Timber.e("article time $articleTime")
                article.publishedAt = articleTime
                holder.itemNewsBinding.article = article
                holder.itemNewsBinding.line1.setOnClickListener { adapterEventListener.onNewsClickListener(article) }
            } else {
                val articleTime = AppUtils.formatDate(article.publishedAt)
                Timber.e("article time $articleTime")
                article.publishedAt = articleTime
                holder.itemNewsBinding.article = article
                holder.itemNewsBinding.line2.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
                holder.itemNewsBinding.line3.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
                holder.itemNewsBinding.line4.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
                holder.itemNewsBinding.line5.setOnClickListener{adapterEventListener.onNewsClickListener(article)}
            }
        }
    }


    inner class NewsViewHolder(val itemNewsBinding: ItemNewsBinding):RecyclerView.ViewHolder(itemNewsBinding.root)
}