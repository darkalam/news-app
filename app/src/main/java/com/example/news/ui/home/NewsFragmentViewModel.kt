package com.example.news.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.example.news.data.local.ArticleDao
import com.example.news.data.local.NewsLocalityDao
import com.example.news.models.Article
import com.example.news.data.remote.responses.ResultWrapper
import com.example.news.data.repository.NewsRepository
import com.example.news.models.NewsLocality
import kotlinx.coroutines.launch
import timber.log.Timber

class NewsFragmentViewModel(private val repository: NewsRepository,
                            private val newsLocalityDao: NewsLocalityDao,
                            private val articleDao: ArticleDao
) :ViewModel(){

    private val recentNewsMutableData:MutableLiveData<List<Article>> = MutableLiveData()
    val recentNewsLiveData:LiveData<List<Article>>
            get() = recentNewsMutableData

    private val newsLocalityLiveLocal=MutableLiveData<NewsLocality>()
    val newsLocalityLocal:LiveData<NewsLocality>
        get() = newsLocalityLiveLocal


    fun getLocalizedNews(countryCode: String){
        viewModelScope.launch {
          when(val resultResponse =repository.getNewsHeadLines(countryCode)) {
                is ResultWrapper.Success ->{
                    val latestNews=resultResponse.data
                    recentNewsMutableData.postValue(latestNews.articles)
                }
              is ResultWrapper.Error->{
                  Timber.e("Error in response ${resultResponse.exception.localizedMessage}")
          }
            }
        }
    }

    fun getAppLocalityLocal(){
        viewModelScope.launch {
            val newLocalityLocal=newsLocalityDao.getAppLocality()
            Timber.e("NEWS LOCALITY LOCAL ${Gson().toJson(newLocalityLocal)}")
            newsLocalityLiveLocal.postValue(newLocalityLocal)
        }
    }

    fun saveBookmark(article: Article){
        viewModelScope.launch {
            articleDao.insertArticle(article)
        }

    }

}