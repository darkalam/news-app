package com.example.news.ui.main

import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.news.R
import com.example.news.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(){
    private var mExitTime: Long = 0

    override fun layoutId(): Int {
        return R.layout.activity_main
    }

    override fun initData() {

    }

    override fun initView() {

        bindViews()

    }

    private fun bindViews() {
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.newsHomeFragment,
            R.id.readNewsFragment))

        val navController: NavController = findNavController(R.id.navHostFragment)
        setupActionBarWithNavController(navController,appBarConfiguration)
        bottomNavBar.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.navHostFragment).navigateUp()
                || super.onSupportNavigateUp()
    }



    override fun start() {

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (System.currentTimeMillis().minus(mExitTime) <= 2000) {
            finish()

        }
        else {
            mExitTime = System.currentTimeMillis()
            Toast.makeText(this,"Click Again To Exit", Toast.LENGTH_SHORT).show()
        }
    }
}
