package com.example.news.ui.home

import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.news.R
import com.example.news.models.Article
import com.example.news.ui.base.BaseFragment
import com.example.news.ui.detail.WebViewActivity
import com.example.news.utils.AppUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_news_home.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class NewsHomeFragment : BaseFragment(), AdapterEventListener {

    val newsFragmentViewModel by viewModel<NewsFragmentViewModel>()
    private lateinit var newsAdapter: NewsAdapter

    override fun getLayoutId(): Int {
        return R .layout.fragment_news_home
    }

    override fun initView() {
        multipleStatusView.showLoading()
    }


    override fun lazyLoad() {
        newsFragmentViewModel.getAppLocalityLocal()
        newsFragmentViewModel.newsLocalityLocal.observe(viewLifecycleOwner, Observer {
            if (it!=null){
                newsFragmentViewModel.getLocalizedNews(it.countryCode)
                observeLatestNews()
            }
        })
    }

    private fun observeLatestNews() {
        newsFragmentViewModel.recentNewsLiveData.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()){
                multipleStatusView.showContent()
                Timber.e("Latest News ${Gson().toJson(it)}")
                mRecyclerView.layoutManager= LinearLayoutManager(requireContext())
                newsAdapter= NewsAdapter(it,this)
                mRecyclerView.adapter=newsAdapter

            }
        })
    }

    override fun onNewsClickListener(article: Article) {
        Timber.e("Item Clicked ${article.author}")
        article.isBookmark = true
        newsFragmentViewModel.saveBookmark(article)
        val builder = CustomTabsIntent.Builder()
        builder.setToolbarColor(ContextCompat.getColor(this.requireContext(), R.color.colorPrimary))
        builder.setShowTitle(true)
        builder.setStartAnimations(this.requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)
        builder.setExitAnimations(this.requireContext(), android.R.anim.fade_in, android.R.anim.fade_out)

        val customTabsIntent = builder.build()
        // check is chrom available
        val packageName = AppUtils.getPackageNameToUse(this.requireContext(), article.url!!)

        when {
            packageName != null -> {
                customTabsIntent.intent.setPackage(packageName)
                customTabsIntent.launchUrl(this.requireContext(), Uri.parse(article.url))
            }
            else -> {
                val webViewIntent = Intent(requireContext(), WebViewActivity::class.java)
                webViewIntent.putExtra("url", article.url)
                startActivity(webViewIntent)
            }
        }
    }
}