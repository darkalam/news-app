package com.example.news.data.remote.responses


import com.google.gson.annotations.SerializedName
import com.example.news.models.Article

data class NewsResponse(
    @SerializedName("articles")
    var articles: List<Article>?,
    @SerializedName("status")
    var status: String?,
    @SerializedName("totalResults")
    var totalResults: Int?
)