package com.example.news.data.local

import androidx.room.*
import com.example.news.models.Article

@Dao
interface ArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArticle(article: Article)

    @Query("SELECT * from `newsArticle`")
    suspend fun getLocalArticles(): List<Article>

    @Delete
    suspend fun deleteArticle(article: Article)
}