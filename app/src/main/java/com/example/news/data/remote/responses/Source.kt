package com.example.news.data.remote.responses


import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.example.news.models.SourcesConverter

@TypeConverters(SourcesConverter::class)
data class Source(
    @SerializedName("id")
    var id: Any?,
    @SerializedName("name")
    var name: String?
)