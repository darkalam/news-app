package com.example.news.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.news.models.NewsLocality

@Dao
interface NewsLocalityDao {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        suspend fun insertNewsLocality(newsLocality: NewsLocality)

        @Query("SELECT * from `newslocality`")
        suspend fun getAppLocality():NewsLocality

}