package com.example.news.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.news.models.Article
import com.example.news.models.NewsLocality
import com.example.news.models.SourcesConverter

@Database(entities = [NewsLocality::class,Article::class], version = 1, exportSchema = false)
@TypeConverters(SourcesConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun newsLocalityDao(): NewsLocalityDao

    abstract fun articleDao():ArticleDao
}