package com.example.news.data.repository

import com.google.gson.Gson
import com.example.news.BuildConfig
import com.example.news.constants.Constants
import com.example.news.data.remote.ApiService
import com.example.news.data.remote.responses.NewsResponse
import com.example.news.data.remote.responses.ResultWrapper
import com.example.news.models.NewsLocality
import timber.log.Timber
import java.lang.Exception

class NewsRepository(private val apiService: ApiService): Repository {

    override suspend fun getNewsHeadLines(countryCode: String):ResultWrapper<NewsResponse>{
          try {
             val dataResponse=apiService.loadHeadlines(countryCode)
             var newsResponse:NewsResponse?=null
             when {
                 dataResponse.isSuccessful -> {
                     Timber.e("Success ${Gson().toJson(dataResponse.body())}")
                     newsResponse= dataResponse.body()!!
                 }
                 else -> {
                     Timber.e("Error ${Gson().toJson(dataResponse.errorBody())}")
                 }
             }
             return ResultWrapper.Success(newsResponse!!)

        }catch (exception:Exception){

            val errorResponse=ResultWrapper.Error(exception)
            Timber.e("Error ${Gson().toJson(errorResponse.exception.localizedMessage)}")
            return ResultWrapper.Error(errorResponse.exception)
        }
    }

    override suspend fun getCategorizedNewsHeadLines(countryCode: String,category:String):ResultWrapper<NewsResponse>{
        try {
            val dataResponse=apiService.loadCategoryHeadlines(countryCode,category)
            var newsResponse:NewsResponse?=null
            when {
                dataResponse.isSuccessful -> {
                    Timber.e("Success ${Gson().toJson(dataResponse.body())}")
                    newsResponse= dataResponse.body()!!
                }
                else -> {
                    Timber.e("Error ${Gson().toJson(dataResponse.errorBody())}")
                }
            }
            return ResultWrapper.Success(newsResponse!!)

        }catch (exception:Exception){

            val errorResponse=ResultWrapper.Error(exception)
            Timber.e("Error ${Gson().toJson(errorResponse.exception.localizedMessage)}")
            return ResultWrapper.Error(errorResponse.exception)
        }
    }

    override suspend fun getApiNewsCountries(): ResultWrapper<List<NewsLocality>>{
        try {
            val countriesResponse=apiService.getNewsApiCountries(BuildConfig.API_NEWS_COUNTRIES+Constants.API_NEWS_CATEGORY_ENDPOINT)
            val countriesList:MutableList<NewsLocality> = mutableListOf()
            when {
                countriesResponse.isSuccessful -> {
                    Timber.e("Success ${Gson().toJson(countriesResponse.body()!!.size)}")
                    countriesList.addAll(countriesResponse.body()!!)
                }
                else -> {
                    Timber.e("Error ${Gson().toJson(countriesResponse.errorBody())}")
                }
            }
            return ResultWrapper.Success(countriesList)
        }catch (exception:Exception){
            val errorResponse=ResultWrapper.Error(exception)
            Timber.e("Error ${Gson().toJson(errorResponse.exception.localizedMessage)}")
            return ResultWrapper.Error(errorResponse.exception)
        }
    }
}