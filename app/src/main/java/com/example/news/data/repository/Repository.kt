package com.example.news.data.repository

import com.example.news.data.remote.responses.NewsResponse
import com.example.news.data.remote.responses.ResultWrapper
import com.example.news.models.NewsLocality


interface Repository{

    suspend fun getNewsHeadLines(countryCode:String):ResultWrapper<NewsResponse>

    suspend fun getCategorizedNewsHeadLines(countryCode:String,category:String):ResultWrapper<NewsResponse>

    suspend fun  getApiNewsCountries():ResultWrapper<List<NewsLocality>>
}